FROM jupyter/datascience-notebook


env CODE_SERVER_VERSION=3.7.4
env SERVICE_URL=https://open-vsx.org/vscode/gallery
env ITEM_URL=https://open-vsx.org/vscode/item

ENV CRAN_URL https://cloud.r-project.org/

USER root

RUN apt-get update -y               \
 && apt-get install -y git          \
 && wget https://github.com/cdr/code-server/releases/download/v${CODE_SERVER_VERSION}/code-server_${CODE_SERVER_VERSION}_amd64.deb                     \
 && dpkg -i ./code-server*.deb                        \
 && rm code-server_${CODE_SERVER_VERSION}_amd64.deb   \
 && apt-get clean

# install rstudio-server
# RUN set -e \
#       && apt-get -y update \
#       && apt-get -y dist-upgrade \
#       && apt-get -y install --no-install-recommends --no-install-suggests \
#         libapparmor1 libclang-dev lsb-release psmisc  \
#       && apt-get -y autoremove \
#       && apt-get clean \
#       && rm -rf /var/lib/apt/lists/*

# ADD https://s3.amazonaws.com/rstudio-server/current.ver /tmp/ver

# RUN ln -s /dev/stdout /var/log/syslog \
#   && cut -f 1 -d - /tmp/ver \
#     | xargs -i curl -SL \
#       https://download2.rstudio.org/server/bionic/amd64/rstudio-server-{}-amd64.deb \
#       -o ./rstudio.deb \
#   && dpkg -i ./rstudio.deb \
#   && rm rstudio.deb  
  
# RUN set -eo pipefail \
#       && useradd -m -d /home/rstudio -g rstudio-server rstudio \
#       && echo rstudio:rstudio | chpasswd \
#       && echo "r-cran-repos=${CRAN_URL}" >> /etc/rstudio/rsession.conf

USER $NB_USER

RUN conda install --quiet --yes \
    'nbgitpuller'  \
 && conda clean --all -f -y && \
    fix-permissions "${CONDA_DIR}" && \
    fix-permissions "/home/${NB_USER}"

run julia -e 'import Pkg;  \
    Pkg.add(url="https://github.com/BENGAL-TIGER/CoolProp-6.3.0.jl.git"); \
    Pkg.add("Unitful");     \
    Pkg.add("Gadfly");      \
    Pkg.add("RDatasets");   \
    Pkg.add("Plots");       \
    Pkg.add("PlotlyJS");    \
    Pkg.precompile();'


# install codeserver extensions
copy extensions/ ${HOME}/tmp/extensions/
run code-server --install-extension ms-python.python                                \
                --install-extension julialang.language-julia-insider                \
                --install-extension ${HOME}/tmp/extensions/styrokai-0.1.8.vsix      \
                --install-extension hediet.vscode-drawio


RUN pip install git+https://github.com/betatim/vscode-binder          \
 && pip install jupyter-rsession-proxy                                \
 && jupyter serverextension enable --py jupyter_server_proxy          \
 && jupyter labextension install --no-build @jupyterlab/server-proxy  \
 && jupyter lab build --minimize=False                                \
 && npm cache clean --force                                           \
 && rm -rf /home/${NB_USER}/.cache  

copy work/ ${HOME}/work/


WORKDIR ${HOME}/work